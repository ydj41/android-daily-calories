package com.example.caloriedaily

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.caloriedaily.entity.DailyCalorie
import com.example.caloriedaily.entity.DailyCalorieCache
import com.example.caloriedaily.utils.GraphUtil
import com.jjoe64.graphview.GraphView
import kotlinx.android.synthetic.main.fragment_first.*
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment(), View.OnClickListener {

    private var beginDate: LocalDate? = null;
    private var endDate: LocalDate? = null;

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val beginClick: TextView? = view?.findViewById(R.id.textview_begin_click)
        val endClick: TextView? = view?.findViewById(R.id.textview_end_click)

        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<Button>(R.id.button_edit).setOnClickListener(this)
        view.findViewById<Button>(R.id.button_query).setOnClickListener(this)
        view.findViewById<TextView>(R.id.textview_begin_click).setOnClickListener(this)
        view.findViewById<TextView>(R.id.textview_end_click).setOnClickListener(this)

        val graph: GraphView = view.findViewById<GraphView>(R.id.graph)
        val data = DailyCalorieCache.getAllData()
        if (data.size != 0)
            GraphUtil.initGraphData(activity, graph, data);
        else {
            graph.visibility = View.INVISIBLE
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.textview_begin_click -> {
                clickEvent("begin")
            }
            R.id.textview_end_click -> {
                clickEvent("end")
            }
            R.id.button_edit -> {
                findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
            }
            R.id.button_query -> {
                queryData()
            }
        }
    }

    fun clickEvent(type: String) {
        val calendar = Calendar.getInstance()
        var mYear = calendar[Calendar.YEAR]
        var mMonth = calendar[Calendar.MONTH]
        var mDay = calendar[Calendar.DAY_OF_MONTH]

        val datePickerDialog = this.activity?.let {
            DatePickerDialog(
                it,
                { _, year, month, dayOfMonth ->
                    mYear = year
                    mMonth = month
                    mDay = dayOfMonth

                    val mDate = "${year}-${month + 1}-${dayOfMonth}"
                    val dateFormat: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-M-d")

                    if (type.equals("begin")) {
                        beginDate = LocalDate.parse(mDate, dateFormat) //将string转为localDate
                        textview_beginDate.text = beginDate.toString()
                    }
                    if (type.equals("end")) {
                        endDate = LocalDate.parse(mDate, dateFormat)
                        textview_endDate.text = endDate.toString()
                    }

                },
                mYear, mMonth, mDay
            )
        }
        if (datePickerDialog != null) {
            datePickerDialog.show()
        };
    }

    fun queryData() {
        if (beginDate == null) {
            textview_info.text = "请选择开始日期"
            return
        }
        if (endDate == null) {
            textview_info.text = "请选择结束日期"
            return
        }
        if (beginDate!!.equals(endDate)) {
            textview_info.text = "开始与结束日期不能是同一天"
            return
        }
        if (beginDate!!.isAfter(endDate)) { //结束日期早于开始日期
            textview_info.text = "开始日期不能晚于结束日期"
            return
        }
        if ((endDate!!.toEpochDay() - beginDate!!.toEpochDay()) > 7) {
            textview_info.text = "跨度不能大于7天"
            return
        }
        textview_info.text = ""
        val dateInterval: LinkedList<DailyCalorie> =
            LinkedList(DailyCalorieCache.getDataByDateInterval(beginDate!!, endDate!!))
        if (dateInterval.size == 0) {
            textview_info.text = "该区间没有数据"
            return
        }
        GraphUtil.initGraphData(activity, graph, dateInterval)
    }
}