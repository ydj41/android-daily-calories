package com.example.caloriedaily

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.example.caloriedaily.entity.DailyCalorie
import com.example.caloriedaily.entity.DailyCalorieCache
import com.example.caloriedaily.entity.DailyCalorieTable
import com.example.caloriedaily.utils.DataBaseUtil
import java.time.LocalDate


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        val db = DataBaseUtil.getDBInstance(applicationContext);
        if (db == null) { //若加载数据库失败, 往内存中插入测试数据
            val d1 = LocalDate.parse("2021-05-15")
            val d2 = LocalDate.parse("2021-05-16")
            val d3 = LocalDate.parse("2021-05-17")
            val d4 = LocalDate.now()

            val dc1 = DailyCalorie(d1, 1201.1)
            val dc2 = DailyCalorie(d2, 2051.3)
            val dc3 = DailyCalorie(d3, 4231.1)
            val dc4 = DailyCalorie(d4, 1231.1)

            DailyCalorieCache.iniAddData(dc1)
            DailyCalorieCache.iniAddData(dc2)
            DailyCalorieCache.iniAddData(dc3)
            DailyCalorieCache.iniAddData(dc4)

        } else { //正常加载数据库
            val tempList = db?.dailyCalorieDao()?.getAll()
            if (tempList != null && tempList.size != 0) {
                for (item in tempList) {
                    DailyCalorieCache.iniAddData(DailyCalorie(LocalDate.parse(item.date), item.calorie!!.toDouble()))
                }
            } else { //成功加载数据库, 但数据库中没有数据, 往数据库中插入数据
                db.dailyCalorieDao()?.insertData(DailyCalorieTable("2021-05-15", "1201.1"))
                db.dailyCalorieDao()?.insertData(DailyCalorieTable("2021-05-16", "2051.3"))
                db.dailyCalorieDao()?.insertData(DailyCalorieTable("2021-05-17", "4231.1"))
                db.dailyCalorieDao()?.insertData(DailyCalorieTable("2021-05-18", "1231.1"))

                if (tempList != null) { //再从数据库中读数据
                    for (item in tempList) {
                        DailyCalorieCache.iniAddData(
                            DailyCalorie(
                                LocalDate.parse(item.date),
                                item.calorie!!.toDouble()
                            )
                        )
                    }
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

}