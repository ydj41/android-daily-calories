package com.example.caloriedaily

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.caloriedaily.entity.DailyCalorie
import com.example.caloriedaily.entity.DailyCalorieCache
import kotlinx.android.synthetic.main.fragment_second.*
import kotlinx.android.synthetic.main.item_text.view.*
import java.time.LocalDate


/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class SecondFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //数组适配器
        val arrayAdapter: ArrayAdapter<DailyCalorie>? =
            context?.let {
                ArrayAdapter(
                    it,
                    R.layout.item_text, //对应xml布局
                    R.id.textView_item, //对应xml内的控件
                    DailyCalorieCache.getAllData() //绑定的数据源
                )
            }

        listview.adapter = arrayAdapter;
        listview.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l -> //长按监听lambok
            val text = view.textView_item.text
            if (text != "") {
                AlertDialog.Builder(this.context).setTitle("确认删除吗？")
                    .setPositiveButton("确定", DialogInterface.OnClickListener { dialog, which -> // 点击“确认”后的操作
                        val dateText = text.split(":")[0]
                        DailyCalorieCache.deleteDataByDate(LocalDate.parse(dateText), this.context!!)
                        arrayAdapter!!.notifyDataSetChanged()
                    })
                    .setNegativeButton("返回", DialogInterface.OnClickListener { dialog, which ->

                    }).show()
            }
        }

        view.findViewById<Button>(R.id.button_second).setOnClickListener {
            findNavController().navigate(R.id.action_SecondFragment_to_ThirdFragment)
        }
    }


}