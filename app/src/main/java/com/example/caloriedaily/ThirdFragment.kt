package com.example.caloriedaily

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.caloriedaily.entity.DailyCalorie
import com.example.caloriedaily.entity.DailyCalorieCache
import kotlinx.android.synthetic.main.fragment_third.*
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*


/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class ThirdFragment : Fragment(), View.OnTouchListener, View.OnClickListener {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_third, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        editTextDate.setOnTouchListener(this)
        button_submit.setOnClickListener(this)

//        view.findViewById<Button>(R.id.button_second).setOnClickListener {
//            findNavController().navigate(R.id.action_Th)
//        }
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        if (event!!.action == MotionEvent.ACTION_DOWN) {
            when (v?.id) {
                R.id.editTextDate -> {
                    showDatePickDlg()
                    return true
                }

            }
        }
        return false
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.button_submit -> {
                if (editTextDate.text.toString().equals("")) {
                    textView_info.text = "请输入日期!"
                    textView_info.setTextColor(android.graphics.Color.RED)
                    return
                }
                if (editTextNumber.text.toString().equals("")) {
                    textView_info.text = "请输入数值!"
                    textView_info.setTextColor(android.graphics.Color.RED)
                    return
                }

                val dateFormat: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-M-d")
                val date: LocalDate = LocalDate.parse(editTextDate.text, dateFormat)
                val calorie: Double? = editTextNumber.text.toString().toDouble()

                if (!DailyCalorieCache.addData(DailyCalorie(date, calorie!!), this.context!!)) {
                    textView_info.text = "该日期已存在数据!"
                    textView_info.setTextColor(android.graphics.Color.RED)
                    return
                } else {
                    textView_info.text = "插入成功!"
                    textView_info.setTextColor(android.graphics.Color.GREEN)
                }

            }
        }
    }

    protected fun showDatePickDlg() {
        val calendar: Calendar = Calendar.getInstance()
        val datePickerDialog = DatePickerDialog(
            this.activity!!,
            { _, year, monthOfYear, dayOfMonth -> this.activity!!.editTextDate.setText("${year}-${monthOfYear + 1}-${dayOfMonth}") },
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        datePickerDialog.show()
    }


}