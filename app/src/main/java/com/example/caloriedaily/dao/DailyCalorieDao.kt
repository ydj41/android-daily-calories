package com.example.caloriedaily.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.caloriedaily.entity.DailyCalorieTable

/**
@author Soap
time 2021-05-19 5:59
 */
@Dao
interface DailyCalorieDao {
    @Query("SELECT * FROM dataTable")
    fun getAll(): Array<DailyCalorieTable>

    @Insert
    fun insertData(dailyCalorieTable: DailyCalorieTable)

    @Query("DELETE FROM dataTable WHERE date=:date")
    fun deleteData(date: String)

}