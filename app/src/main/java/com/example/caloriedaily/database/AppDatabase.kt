package com.example.caloriedaily.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.caloriedaily.dao.DailyCalorieDao
import com.example.caloriedaily.entity.DailyCalorieTable

/**
@author Soap
time 2021-05-19 6:06
 */
@Database(entities = [DailyCalorieTable::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun dailyCalorieDao(): DailyCalorieDao?
}