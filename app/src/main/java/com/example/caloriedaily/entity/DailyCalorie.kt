package com.example.caloriedaily.entity

import com.example.caloriedaily.utils.LocalDateUtil
import com.jjoe64.graphview.series.DataPoint
import java.time.LocalDate
import java.util.*

/**
@author Soap
time 2021-05-18 1:46
 */
class DailyCalorie : Comparable<DailyCalorie> {
    var date: LocalDate? = null;
    var calorie: Double = 0.0;

    init {

    }

    constructor(date: LocalDate, calorie: Double) {
        this.date = date;
        this.calorie = calorie;
    }

    override fun compareTo(o: DailyCalorie): Int { //重载比较
        if (this.date?.equals(o.date) == true) return 0;
        if (this.date?.isAfter(o.date) == true) return 1;
        return -1;
    }

    override fun toString(): String {
        return this.date.toString() + ": " + calorie.toString();
    }

    companion object {
        //把每日卡路里数转换为DataPonit数组
        fun convertDataPoint(dailyCalories: LinkedList<DailyCalorie>): ArrayList<DataPoint>? {
            var dataPoints: ArrayList<DataPoint>? = ArrayList()

            for (item in dailyCalories) {
                dataPoints?.add(DataPoint(LocalDateUtil.localDate2Date(item.date!!), item.calorie)); //将LocalDate转化为Date
            }

            return dataPoints
        }
    }
}