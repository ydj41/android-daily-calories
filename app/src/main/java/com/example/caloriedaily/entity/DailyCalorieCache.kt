package com.example.caloriedaily.entity

import android.content.Context
import com.example.caloriedaily.utils.DataBaseUtil
import java.time.LocalDate
import java.util.*

/**
@author Soap
time 2021-05-18 19:29
 */
object DailyCalorieCache {
    private var dailyCalorieList: LinkedList<DailyCalorie> = LinkedList()

    fun getDataByDate(date: LocalDate): DailyCalorie? {
        for (item in dailyCalorieList) {
            if (item.date?.equals(date) == true) {
                return item
            }
        }
        return null
    }

    fun isDateExist(date: LocalDate): Boolean {
        for (item in dailyCalorieList) {
            if (item.date!!.equals(date))
                return true
        }
        return false
    }

    fun getDataByDateInterval(beginDate: LocalDate, endDate: LocalDate): LinkedList<DailyCalorie>? {
        var res: LinkedList<DailyCalorie> = LinkedList()
        for (item in dailyCalorieList) {
            if (item.date!! in beginDate..endDate)
                res.add(item)
        }
        res.sort() //按日期排序
        return res
    }

    fun getAllData(): LinkedList<DailyCalorie> {
        return dailyCalorieList
    }

    fun addData(data: DailyCalorie, context: Context): Boolean {
        if (this.isDateExist(data.date!!))
            return false
        this.dailyCalorieList.add(data)
        this.dailyCalorieList.sort()

        val db = DataBaseUtil.getDBInstance(context);
        db?.dailyCalorieDao()?.insertData(DailyCalorieTable(data.date.toString(), data.calorie.toString()))
        return true
    }

    fun iniAddData(data: DailyCalorie): Boolean {
        if (this.isDateExist(data.date!!))
            return false
        this.dailyCalorieList.add(data)
        this.dailyCalorieList.sort()

        return true
    }

    fun deleteDataByDate(date: LocalDate, context: Context): Boolean {
        for (item in dailyCalorieList) {
            if (item.date!!.equals(date)) {
                dailyCalorieList.remove(item)
                this.dailyCalorieList.sort()

                val db = DataBaseUtil.getDBInstance(context);
                db?.dailyCalorieDao()?.deleteData(date.toString()   )
                return true
            }
        }
        return false
    }

    fun cloneData(data: LinkedList<DailyCalorie>) {
        this.dailyCalorieList = data.clone() as LinkedList<DailyCalorie>
    }
}