package com.example.caloriedaily.entity

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
@author Soap
time 2021-05-19 5:54
 */
@Entity(tableName = "dataTable")
data class DailyCalorieTable(@PrimaryKey var date: String, var calorie: String?)



