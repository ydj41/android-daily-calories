package com.example.caloriedaily.utils

import android.content.Context
import androidx.room.Room
import com.example.caloriedaily.database.AppDatabase

/**
@author Soap
time 2021-05-19 7:47
 */
object DataBaseUtil {
    private var databaseInstance: AppDatabase? = null

    @Synchronized
    fun getDBInstance(context: Context): AppDatabase? {
        if (databaseInstance == null) {
            databaseInstance =
                Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "database-name")
                    .allowMainThreadQueries()
                    .build()
        }
        return databaseInstance
    }
}