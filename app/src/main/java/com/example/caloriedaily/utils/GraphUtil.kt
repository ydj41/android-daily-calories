package com.example.caloriedaily.utils

import androidx.fragment.app.FragmentActivity
import com.example.caloriedaily.entity.DailyCalorie
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import java.util.*

/**
@author Soap
time 2021-05-18 2:28
 */
class GraphUtil {

    companion object {
        //初始化一个图表
        fun initGraphData(
            fragmentActivity: FragmentActivity?,
            graph: GraphView,
            dailyCalories: LinkedList<DailyCalorie>
        ) {
            val series: LineGraphSeries<DataPoint> = LineGraphSeries()
            val dataPoints = DailyCalorie.convertDataPoint(dailyCalories)

            if (dataPoints != null) {
                for (item in dataPoints) {
                    series.appendData(item, false, dailyCalories.size)
                }
            }
            graph.removeAllSeries()
            graph.addSeries(series)
            graph.gridLabelRenderer.labelFormatter = DateAsXAxisLabelFormatter(fragmentActivity);

            if (dailyCalories.size == 1) { //对只有一条数据时做特殊处理, 显示开始和结束日期相同的一条直线
                graph.gridLabelRenderer.numHorizontalLabels = 2
                graph.viewport.setMinX(LocalDateUtil.localDate2Date(dailyCalories[0].date!!).time.toDouble())
                graph.viewport.setMaxX(LocalDateUtil.localDate2Date(dailyCalories[0].date!!).time.toDouble())
            } else {
                graph.gridLabelRenderer.numHorizontalLabels =
                    if (dailyCalories.size <= 4) dailyCalories.size else 4 //x轴最大数据量为4, 不显示中间的数据
                graph.viewport.setMinX(LocalDateUtil.localDate2Date(dailyCalories[0].date!!).time.toDouble())
                graph.viewport.setMaxX(LocalDateUtil.localDate2Date(dailyCalories[dailyCalories.size - 1].date!!).time.toDouble())
            }

            graph.viewport.isXAxisBoundsManual = true;
            graph.gridLabelRenderer.isHumanRounding = false;
        }
    }
}