package com.example.caloriedaily.utils

import java.time.LocalDate
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.*

/**
@author Soap
time 2021-05-18 20:16
 */
object LocalDateUtil {
    fun localDate2Date(temp: LocalDate): Date {
        val zdt: ZonedDateTime = temp!!.atStartOfDay(ZoneId.systemDefault()) //将LocalDate转化为Date
        return Date.from(zdt.toInstant())
    }
}